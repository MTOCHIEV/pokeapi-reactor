[![CD Pipeline](https://github.com/SirSkaro/pokeapi-reactor/actions/workflows/maven-publish.yml/badge.svg?branch=v1.0.2)](https://github.com/SirSkaro/pokeapi-reactor/actions/workflows/maven-publish.yml)
![Coverage](.github/badges/jacoco.svg)
![Branches](.github/badges/branches.svg)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# this is a fork of pokeapi-reactor, adding PalParkEncounters and related endpoints
https://github.com/SirSkaro/pokeapi-reactor


# pokeapi-reactor
A non-blocking, reactive API client for [PokeAPI](https://pokeapi.co/) with caching for Spring Boot projects.
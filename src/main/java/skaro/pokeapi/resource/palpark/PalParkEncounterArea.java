package skaro.pokeapi.resource.palpark;

import skaro.pokeapi.resource.NamedApiResource;

public class PalParkEncounterArea {

    private NamedApiResource<PalParkArea> area;
    private int baseScore;
    private int rate;

    public NamedApiResource<PalParkArea> getArea() {
        return area;
    }

    public void setArea(NamedApiResource<PalParkArea> area) {
        this.area = area;
    }

    public int getBaseScore() {
        return baseScore;
    }

    public void setBaseScore(int baseScore) {
        this.baseScore = baseScore;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

}

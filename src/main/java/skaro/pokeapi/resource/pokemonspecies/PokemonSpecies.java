package skaro.pokeapi.resource.pokemonspecies;

import skaro.pokeapi.resource.*;
import skaro.pokeapi.resource.egggroup.EggGroup;
import skaro.pokeapi.resource.evolutionchain.EvolutionChain;
import skaro.pokeapi.resource.generation.Generation;
import skaro.pokeapi.resource.growthrate.GrowthRate;
import skaro.pokeapi.resource.palpark.PalParkEncounterArea;
import skaro.pokeapi.resource.pokemoncolor.PokemonColor;
import skaro.pokeapi.resource.pokemonhabitat.PokemonHabitat;
import skaro.pokeapi.resource.pokemonshape.PokemonShape;
import skaro.pokeapi.utils.locale.Localizable;

import java.util.List;

public class PokemonSpecies implements PokeApiResource, Localizable {

    private Integer baseHappiness;
    private Integer captureRate;
    private NamedApiResource<PokemonColor> color;
    private List<NamedApiResource<EggGroup>> eggGroups;
    private ApiResource<EvolutionChain> evolutionChain;
    private NamedApiResource<PokemonSpecies> evolvesFromSpecies;
    private List<FlavorText> flavorTextEntries;
    private List<Description> formDescriptions;
    private Boolean formsSwitchable;
    private Integer genderRate;
    private List<Genus> genera;
    private NamedApiResource<Generation> generation;
    private NamedApiResource<GrowthRate> growthRate;
    private NamedApiResource<PokemonHabitat> habitat;
    private Boolean hasGenderDifferences;
    private Integer hatchCounter;
    private Integer id;
    private Boolean isBaby;
    private Boolean isLegendary;
    private Boolean isMythical;
    private String name;
    private List<Name> names;
    private Integer order;
    private List<PalParkEncounterArea> pal_park_encounters;
    private List<PokemonSpeciesDexEntry> pokedexNumbers;
    private NamedApiResource<PokemonShape> shape;
    private List<PokemonSpeciesVariety> varieties;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getGenderRate() {
        return genderRate;
    }

    public void setGenderRate(Integer genderRate) {
        this.genderRate = genderRate;
    }

    public Integer getCaptureRate() {
        return captureRate;
    }

    public void setCaptureRate(Integer captureRate) {
        this.captureRate = captureRate;
    }

    public Integer getBaseHappiness() {
        return baseHappiness;
    }

    public void setBaseHappiness(Integer baseHappiness) {
        this.baseHappiness = baseHappiness;
    }

    public Boolean getIsBaby() {
        return isBaby;
    }

    public void setIsBaby(Boolean isBaby) {
        this.isBaby = isBaby;
    }

    public Boolean getIsLegendary() {
        return isLegendary;
    }

    public void setIsLegendary(Boolean isLegendary) {
        this.isLegendary = isLegendary;
    }

    public Boolean getIsMythical() {
        return isMythical;
    }

    public void setIsMythical(Boolean isMythical) {
        this.isMythical = isMythical;
    }

    public Integer getHatchCounter() {
        return hatchCounter;
    }

    public void setHatchCounter(Integer hatchCounter) {
        this.hatchCounter = hatchCounter;
    }

    public Boolean getHasGenderDifferences() {
        return hasGenderDifferences;
    }

    public void setHasGenderDifferences(Boolean hasGenderDifferences) {
        this.hasGenderDifferences = hasGenderDifferences;
    }

    public Boolean getFormsSwitchable() {
        return formsSwitchable;
    }

    public void setFormsSwitchable(Boolean formsSwitchable) {
        this.formsSwitchable = formsSwitchable;
    }

    public NamedApiResource<GrowthRate> getGrowthRate() {
        return growthRate;
    }

    public void setGrowthRate(NamedApiResource<GrowthRate> growthRate) {
        this.growthRate = growthRate;
    }

    public List<PokemonSpeciesDexEntry> getPokedexNumbers() {
        return pokedexNumbers;
    }

    public void setPokedexNumbers(List<PokemonSpeciesDexEntry> pokedexNumbers) {
        this.pokedexNumbers = pokedexNumbers;
    }

    public List<NamedApiResource<EggGroup>> getEggGroups() {
        return eggGroups;
    }

    public void setEggGroups(List<NamedApiResource<EggGroup>> eggGroups) {
        this.eggGroups = eggGroups;
    }

    public NamedApiResource<PokemonColor> getColor() {
        return color;
    }

    public void setColor(NamedApiResource<PokemonColor> color) {
        this.color = color;
    }

    public NamedApiResource<PokemonShape> getShape() {
        return shape;
    }

    public void setShape(NamedApiResource<PokemonShape> shape) {
        this.shape = shape;
    }

    public NamedApiResource<PokemonSpecies> getEvolvesFromSpecies() {
        return evolvesFromSpecies;
    }

    public void setEvolvesFromSpecies(NamedApiResource<PokemonSpecies> evolvesFromSpecies) {
        this.evolvesFromSpecies = evolvesFromSpecies;
    }

    public ApiResource<EvolutionChain> getEvolutionChain() {
        return evolutionChain;
    }

    public void setEvolutionChain(ApiResource<EvolutionChain> evolutionChain) {
        this.evolutionChain = evolutionChain;
    }

    public NamedApiResource<PokemonHabitat> getHabitat() {
        return habitat;
    }

    public void setHabitat(NamedApiResource<PokemonHabitat> habitat) {
        this.habitat = habitat;
    }

    public NamedApiResource<Generation> getGeneration() {
        return generation;
    }

    public void setGeneration(NamedApiResource<Generation> generation) {
        this.generation = generation;
    }

    public List<Name> getNames() {
        return names;
    }

    public void setNames(List<Name> names) {
        this.names = names;
    }

    public List<FlavorText> getFlavorTextEntries() {
        return flavorTextEntries;
    }

    public void setFlavorTextEntries(List<FlavorText> flavorTextEntries) {
        this.flavorTextEntries = flavorTextEntries;
    }

    public List<Description> getFormDescriptions() {
        return formDescriptions;
    }

    public void setFormDescriptions(List<Description> formDescriptions) {
        this.formDescriptions = formDescriptions;
    }

    public List<Genus> getGenera() {
        return genera;
    }

    public void setGenera(List<Genus> genera) {
        this.genera = genera;
    }

    public List<PokemonSpeciesVariety> getVarieties() {
        return varieties;
    }

    public void setVarieties(List<PokemonSpeciesVariety> varieties) {
        this.varieties = varieties;
    }

    public List<PalParkEncounterArea> getPalParkEncounters() {
        return pal_park_encounters;
    }

    public void setPalParkEncounters(List<PalParkEncounterArea> pal_park_encounters) {
        this.pal_park_encounters = pal_park_encounters;
    }

}

package skaro.pokeapi.resource.palpark;

import skaro.pokeapi.resource.Name;
import skaro.pokeapi.resource.PokeApiResource;
import skaro.pokeapi.utils.locale.Localizable;

import java.util.List;

public class PalParkArea implements PokeApiResource, Localizable {

    private Integer id;
    private String name;
    private List<Name> names;
    private List<PalParkEncounterSpecies> pokemon_encounters;


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNames(List<Name> names) {
        this.names = names;
    }

    public List<PalParkEncounterSpecies> getPokemon_encounters() {
        return pokemon_encounters;
    }

    public void setPokemon_encounters(List<PalParkEncounterSpecies> pokemon_encounters) {
        this.pokemon_encounters = pokemon_encounters;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getName() {
       return name;
    }

    @Override
    public List<Name> getNames() {
        return names;
    }
}

package skaro.pokeapi.resource.palpark;

import skaro.pokeapi.resource.NamedApiResource;
import skaro.pokeapi.resource.pokemonspecies.PokemonSpecies;

public class PalParkEncounterSpecies {

    private int base_score;
    private NamedApiResource<PokemonSpecies> pokemon_species;
    private int rate;

    public NamedApiResource<PokemonSpecies> getPokemon_species() {
        return pokemon_species;
    }

    public void setPokemon_species(NamedApiResource<PokemonSpecies> pokemon_species) {
        this.pokemon_species = pokemon_species;
    }

    public int getBase_score() {
        return base_score;
    }

    public void setBase_score(int base_score) {
        this.base_score = base_score;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

}
